export default function RootLoading() {
  return (
    <div className="fixed inset-0 flex flex-col items-center justify-center bg-secondary-dark z-50">
      <div className="relative w-24 h-24">
        <div className="absolute inset-0 rounded-full border-4 border-primary-light/20"></div>
        <div className="absolute inset-0 rounded-full border-4 border-primary-light border-t-transparent animate-spin"></div>
      </div>
      <h2 className="mt-8 text-xl font-serif text-white">The Way Coffee House</h2>
      <p className="mt-2 text-sm text-white/60">Loading a fresh experience...</p>
    </div>
  );
}