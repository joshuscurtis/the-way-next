import React from "react";
import EventsSection from "./components/EventsSection";
import ContactSection from "./components/ContactSection";
import Footer from "./components/Footer";
import GallerySection from "./components/GallerySection";
import HeroSection from "./components/HeroSection";
import { Suspense } from "react";
import LoadingEvents from "./components/EventsSectionLoading";
import LoadingGallery from "./components/GallerySectionLoading";
import type { Metadata } from "next";

// Cache this page for faster performance
export const revalidate = 3600; // Revalidate once per hour

// Additional metadata for the homepage
export const metadata: Metadata = {
  alternates: {
    canonical: '/',
  },
  openGraph: {
    title: "The Way Coffee House | Award-Winning Coffee in Dunstable",
    description: "Visit our welcoming coffee house for award-winning Fairtrade coffee, homemade cakes and a vibrant community space with regular events.",
    url: 'https://theway.coffee',
    siteName: 'The Way Coffee House',
    images: [
      {
        url: "/min/coffee-min.webp",
        width: 1200,
        height: 630,
        alt: "The Way Coffee House interior",
      }
    ],
  },
};

export default function Home() {
  return (
    <main className="min-h-screen bg-gradient-to-b from-neutral-light to-white font-sans overscroll-none">
      <HeroSection />
      <section id="events" aria-label="Upcoming events">
        <Suspense fallback={<LoadingEvents />}>
          <EventsSection />
        </Suspense>
      </section>
      <section id="gallery" aria-label="Photo gallery">
        <Suspense fallback={<LoadingGallery />}>
          <GallerySection />
        </Suspense>
      </section>
      <ContactSection />
      <Footer />
    </main>
  );
}
