import { Event, FrequencyType } from '@/types/event';
import {
    BookOpen,
    Gamepad2,
    HelpCircle,
    Cake,
    Palette,
    Scissors,
    Volleyball,
} from 'lucide-react';

/**
 * Central data store for all events
 * This is the single source of truth for events in the application
 */
const events: Event[] = [
    {
        id: 'game-on',
        title: 'Game On!',
        dates: 'First Wednesday',
        date: '2024',
        startTime: '19:30',
        endTime: '21:00',
        location: 'The Way Coffee House',
        description: 'Game On! is for anyone who likes playing board games and meet with friends old and new while enjoying a selection of drinks and snacks from The Way Coffee House. A selection of games are provided and you are welcome to bring your own.',
        frequency: 'monthly',
        icon: Gamepad2,
        imageUrl: '/events/game-on.jpg',
        imageAlt: 'People enjoying board games at The Way Coffee House with coffee and snacks on the table',
        richDescription: 'A monthly event for anyone who enjoys playing board games! Play a wide variety of board games suitable for all skill levels. Enjoy a selection of drinks and snacks from our coffee house menu.\n\nWe provide a diverse selection of games including strategy games, card games, and family favorites. You\'re also welcome to bring your own games to share with others.\n\n No booking required, just drop in and join the fun!',
    },
    {
        id: 'book-club',
        title: 'Book Club',
        dates: 'Second Wednesday',
        date: '2024',
        startTime: '19:45',
        endTime: '21:00',
        location: 'The Way Coffee House',
        description: 'From Asimov to Hemmingway, the fictional to the factual, the tear-jerkers to the comedies, Book club enjoys reading old favourites, modern classics, and the yet to be acknowledged!',
        frequency: 'monthly',
        icon: BookOpen,
        imageUrl: '/events/book-club.jpeg'
    },
    {
        id: 'quiz-night',
        title: 'Quiz Night',
        dates: 'Third Wednesday',
        date: '2024',
        startTime: '19:30',
        endTime: '21:00',
        location: 'The Way Coffee House',
        description: 'Join us for a night of fun and friendly competition. Test your knowledge and enjoy a great evening with friends.',
        frequency: 'monthly',
        icon: HelpCircle,
        imageUrl: '/events/quiz-night.jpg'
    },
    {
        id: 'puddings-night',
        title: 'Puddings Night',
        dates: 'Fourth Wednesday',
        date: '2024',
        startTime: '19:30',
        endTime: '21:00',
        location: 'The Way Coffee House',
        description: 'A night of indulgence! Join us for a selection of homemade puddings and a great evening with friends.',
        frequency: 'monthly',
        icon: Cake,
        imageUrl: '/events/pudding.webp'
    },
    {
        id: 'needle-and-thread',
        title: 'Needle & Thread',
        dates: 'First & Third Monday',
        date: '2024',
        startTime: '10:00',
        endTime: '12:30',
        location: "Christ Church Dunstable's Foyer",
        description: "Whether you're a master tailor, a skilled-seamstress, or you simply wouldn't know the eye from the point; Patchwork & Quilting has something for every level of skill.",
        frequency: 'monthly',
        icon: Scissors,
        imageUrl: '/events/needle-and-thread.jpg'
    },
    {
        id: 'art-group',
        title: 'Art Group',
        dates: 'Tuesday',
        date: '2024',
        startTime: '10:00',
        endTime: '12:00',
        location: 'The Way Coffee House',
        description: "Portraits, landscapes, seascapes escapes. Whether it'll be the first time you've putting pencil to paper, or you've just lacquered your latest masterpiece, 'Art' is a group made for you.",
        frequency: 'term time',
        icon: Palette,
        imageUrl: '/events/art.webp'
    },
    {
        id: 'knit-and-natter',
        title: 'Knit & Natter',
        dates: 'Friday',
        date: '2024',
        startTime: '9:30',
        endTime: '11:30',
        location: "Christ Church Dunstable's Foyer",
        description: 'Warm group full of fun, laughter, chatting, and occasionally some knitting gets done. Home to over 40 knitters and crochet-ers ranging from beginner to expert.',
        frequency: 'weekly',
        icon: Volleyball,
        imageUrl: '/events/knit-and-natter.jpeg`'
    }
];

/**
 * Get all events
 * @returns Array of all events
 */
export function getAllEvents(): Event[] {
    return events;
}

/**
 * Get a specific event by ID
 * @param id Event ID
 * @returns Event object or undefined if not found
 */
export function getEvent(id: string): Event | undefined {
    return events.find(event => event.id === id);
}

/**
 * Format the frequency text for display
 * @param dates Date string (e.g., "First Wednesday", "Friday")
 * @param frequency Frequency type 
 * @returns Formatted string for display
 */
export function formatFrequency(dates: string, frequency: FrequencyType): string {
    if (frequency === "weekly") return "Every " + dates;
    if (frequency === "term time") return dates + " during term time";
    if (frequency === "monthly" && dates.includes("&"))
        return dates + " of each month";
    return `${dates} of each month`;
}