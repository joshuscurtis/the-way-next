export default function LoadingEvents() {
  return (
    <section
      id="events"
      className="py-16 px-6 md:px-8 bg-gradient-to-b from-neutral-dark/90 to-neutral-dark"
    >
      <div className="max-w-6xl mx-auto">
        <div className="h-14 w-64 bg-gray-700/50 rounded-xl mx-auto mb-12 animate-pulse" />
        
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          {/* Generate skeleton event cards */}
          {Array.from({ length: 4 }).map((_, i) => (
            <div
              key={i}
              className="p-8 rounded-2xl border border-white/10 
                bg-white/5 backdrop-blur-lg animate-pulse"
            >
              <div className="flex items-start gap-6 mb-6">
                <div className="w-16 h-16 rounded-xl bg-gray-700/50" />
                <div className="flex-1">
                  <div className="h-7 w-3/4 bg-gray-700/50 rounded-lg mb-4" />
                  <div className="space-y-2">
                    <div className="h-4 bg-gray-700/30 rounded-lg w-full" />
                    <div className="h-4 bg-gray-700/30 rounded-lg w-5/6" />
                    <div className="h-4 bg-gray-700/30 rounded-lg w-4/5" />
                  </div>
                </div>
              </div>

              <div className="space-y-3">
                <div className="h-12 bg-gray-700/20 rounded-xl w-full" />
                <div className="h-12 bg-gray-700/20 rounded-xl w-full" />
                <div className="h-12 bg-gray-700/20 rounded-xl w-full" />
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
