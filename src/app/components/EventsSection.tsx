import React from "react";
import { Calendar, Clock, MapPin } from "lucide-react";
import Link from "next/link";
import Image from "next/image";
import { getAllEvents, formatFrequency } from "@/data/events";

// We've removed "use client" directive to make this a server component
export default function EventsSection() {
  // Get events from the unified data source
  const events = getAllEvents();

  return (
    <section
      id="events"
      className="py-12 sm:py-16 px-4 sm:px-6 md:px-8 bg-gradient-to-b from-neutral-dark/90 to-neutral-dark"
    >
      <div className="max-w-6xl mx-auto">
        <h2 className="text-3xl xs:text-4xl md:text-5xl font-serif font-bold mb-4 sm:mb-6 text-center text-white relative">
          Our Events
          <div className="relative">
            <span className="absolute -bottom-3 sm:-bottom-4 left-1/2 transform -translate-x-1/2 w-20 sm:w-24 h-1 bg-primary-light rounded-full"></span>
          </div>
        </h2>
        <p className="text-white/80 text-base sm:text-lg max-w-2xl mx-auto mt-6 mb-8 sm:mb-12 text-center">
          Join us for regular events throughout the month
        </p>

        <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 sm:gap-8 sm:[&>*:last-child:nth-child(odd)]:col-span-2 sm:[&>*:last-child:nth-child(odd)]:mx-auto sm:[&>*:last-child:nth-child(odd)]:max-w-2xl">
          {events.map((event) => (
            <Link
              href={`/events/${event.id}`}
              key={event.id}
              className="group block p-4 xs:p-5 sm:p-6 md:p-8 rounded-xl sm:rounded-2xl border border-white/10 
              hover:border-primary-light transition-all duration-300 
              bg-white/5 backdrop-blur-lg hover:bg-white/10
              hover:translate-y-[-4px] hover:shadow-xl"
            >
              <div className="relative h-48 w-full overflow-hidden">
                {event.imageUrl && (
                  <Image
                    src={event.imageUrl}
                    alt={event.imageAlt || `${event.title} event at The Way Coffee House`}
                    fill
                    className="object-cover transition-transform duration-500 group-hover:scale-110"

                  />
                )}
                <div className="absolute inset-0 bg-gradient-to-t from-black/80 to-transparent"></div>
              </div>

              <div className="flex items-start gap-4 sm:gap-6 mb-4 sm:mb-6 relative">
                <div className="w-12 h-12 xs:w-14 xs:h-14 sm:w-16 sm:h-16 rounded-lg sm:rounded-xl bg-primary-light/10 border border-primary-light/30 flex items-center justify-center group-hover:bg-primary-light/20 transition-all">
                  {event.icon && <event.icon size={24} className="text-primary-light" />}
                </div>
                <div className="flex-1">
                  <h3 className="text-lg xs:text-xl md:text-2xl font-bold text-white group-hover:text-primary-light transition-colors mb-1 sm:mb-2">
                    {event.title}
                  </h3>
                  <p className="text-white/80 text-sm xs:text-base leading-relaxed line-clamp-2 sm:line-clamp-3">
                    {event.description}
                  </p>
                </div>
              </div>

              <div className="space-y-2 sm:space-y-3 text-sm xs:text-base">
                <div className="flex items-center gap-2 sm:gap-3 text-white/90 bg-white/5 p-2 sm:p-3 rounded-lg sm:rounded-xl">
                  <Calendar
                    className="text-primary-light flex-shrink-0"
                    size={18}
                  />
                  <span className="font-medium">
                    {formatFrequency(event.dates, event.frequency)}
                  </span>
                </div>
                <div className="flex items-center gap-2 sm:gap-3 text-white/90 bg-white/5 p-2 sm:p-3 rounded-lg sm:rounded-xl">
                  <Clock className="text-primary-light flex-shrink-0" size={18} />
                  <span className="font-medium">
                    {event.startTime} - {event.endTime}
                  </span>
                </div>
                <div className="flex items-center gap-2 sm:gap-3 text-white/90 bg-white/5 p-2 sm:p-3 rounded-lg sm:rounded-xl">
                  <MapPin
                    className="text-primary-light flex-shrink-0"
                    size={18}
                  />
                  <span className="font-medium">{event.location}</span>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </section>
  );
}