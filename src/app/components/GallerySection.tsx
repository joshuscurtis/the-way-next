"use client";
import Image from "next/image";
import { useState, useEffect } from "react";

// Function to return descriptive alt text for gallery images
function getGalleryImageAlt(imageNum: number): string {
  const altTexts = {
    1: "Coffee being poured into a white mug with latte art formation",
    2: "Interior view of The Way Coffee House with comfortable seating and warm lighting",
    3: "Barista preparing specialty coffee at the coffee bar",
    4: "Selection of pastries and cakes displayed on wooden serving board",
    5: "Customers enjoying conversation over coffee at a window table",
    6: "Close-up of a cappuccino with heart-shaped latte art",
    7: "Book club meeting with people discussing books over coffee",
    8: "Coffee beans in a burlap bag with scoop",
    9: "Outside view of The Way Coffee House entrance with signage"
  };
  
  return altTexts[imageNum as keyof typeof altTexts] || `The Way Coffee House gallery image ${imageNum}`;
}

const GallerySection = () => {
  const [isClient, setIsClient] = useState(false);
  
  useEffect(() => {
    setIsClient(true);
  }, []);

  return (
    <section
      id="gallery"
      className="py-12 sm:py-16 md:py-20 px-4 sm:px-6 md:px-8 bg-gradient-to-b from-neutral-dark/90 to-neutral-dark"
    >
      <div className="max-w-6xl mx-auto">
        <h2 className="text-3xl xs:text-4xl md:text-5xl font-serif font-bold text-center text-white mb-10 sm:mb-16 relative">
          Gallery
          <div className="absolute -bottom-4 left-1/2 transform -translate-x-1/2 w-16 sm:w-20 h-1 bg-primary-light rounded-full"></div>
        </h2>
        
        {isClient && (
          <div className="grid grid-cols-2 xs:grid-cols-3 gap-2 xs:gap-3 md:gap-6 max-w-4xl mx-auto">
            {Array.from({ length: 9 }, (_, i) => i + 1).map((num) => (
              <div
                key={num}
                className="relative aspect-square overflow-hidden rounded-lg sm:rounded-xl 
                  group hover:shadow-xl transition-all duration-300
                  border border-white/10 hover:border-primary-light/30
                  bg-white/5 backdrop-blur-lg transform hover:scale-105"
              >
                <Image
                  src={`/insta/insta_${num}.webp`}
                  alt={getGalleryImageAlt(num)}
                  fill
                  loading="lazy"
                  sizes="(max-width: 640px) 50vw, (max-width: 768px) 33vw, 300px"
                  className="object-cover group-hover:scale-110 
                  transition-all duration-700"
                />
                <div
                  className="absolute inset-0 bg-gradient-to-t from-black/80 to-transparent 
                    opacity-0 group-hover:opacity-100 transition-all duration-300
                    flex flex-col justify-end p-3 sm:p-4 md:p-6"
                />
              </div>
            ))}
          </div>
        )}
        
        {!isClient && (
          <div className="grid grid-cols-2 xs:grid-cols-3 gap-2 xs:gap-3 md:gap-6 max-w-4xl mx-auto">
            {Array.from({ length: 9 }, (_, i) => (
              <div
                key={i}
                className="aspect-square rounded-lg sm:rounded-xl bg-white/5 border border-white/10"
              />
            ))}
          </div>
        )}
      </div>
    </section>
  );
};

export default GallerySection;