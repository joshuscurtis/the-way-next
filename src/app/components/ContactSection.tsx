import React from "react";
import {
  FaClock,
  FaPhone,
  FaMapMarkerAlt,
  FaEnvelope,
  FaInstagram,
  FaFacebook,
  FaYoutube,
} from "react-icons/fa";

const OPENING_HOURS = [
  { day: "Monday", time: "8:30am-3pm" },
  { day: "Tuesday", time: "8:30am-3pm" },
  { day: "Wednesday", time: "8:30am-9pm", isLate: true },
  { day: "Thursday", time: "8:30am-3pm" },
  { day: "Friday", time: "8:30am-3pm" },
  { day: "Saturday", time: "8:30am-3pm" },
  { day: "Sunday", time: "Church 11:15am", isSunday: true },
];

const CONTACT_INFO = [
  {
    icon: <FaMapMarkerAlt className="text-primary-light" />,
    title: "Address",
    content: "6 West Street, Dunstable, Bedfordshire, LU6 1JL",
    href: "https://maps.app.goo.gl/3i2kFJFMd6dJLm5G9",
    ariaLabel: "Find us on Google Maps",
  },
  {
    icon: <FaPhone className="text-primary-light" />,
    title: "Phone",
    content: "01582 661621",
    href: "tel:01582661621",
    ariaLabel: "Call us",
  },
  {
    icon: <FaEnvelope className="text-primary-light" />,
    title: "Email",
    content: "mail@christchurchdunstable.org.uk",
    href: "mailto:mail@christchurchdunstable.org.uk",
    ariaLabel: "Email us",
  },
];

const SOCIAL_LINKS = [
  {
    icon: <FaInstagram />,
    href: "https://instagram.com/the_way_dunstable",
    label: "Instagram",
    ariaLabel: "Follow us on Instagram",
  },
  {
    icon: <FaFacebook />,
    href: "https://facebook.com/thewaydunstable",
    label: "Facebook",
    ariaLabel: "Like us on Facebook",
  },
  {
    icon: <FaYoutube />,
    href: "https://youtube.com/@christchurchdunstable",
    label: "YouTube",
    ariaLabel: "Watch us on YouTube",
  },
];

const ContactSection = () => {
  const InfoCard = ({
    title,
    icon,
    children,
    className = "",
  }: {
    title: string;
    icon: React.ReactNode;
    children: React.ReactNode;
    className?: string;
  }) => (
    <div
      className={`bg-white/5 backdrop-blur-lg p-4 sm:p-6 lg:p-8 rounded-2xl border border-white/10 hover:border-[#D4A574]/30 transition-all duration-300 overflow-hidden ${className}`}
    >
      <h3 className="text-lg sm:text-xl lg:text-2xl font-bold mb-4 sm:mb-6 text-white flex items-center gap-3">
        <div className="w-8 h-8 sm:w-10 sm:h-10 rounded-full bg-[#D4A574]/10 flex items-center justify-center shrink-0">
          {icon}
        </div>
        {title}
      </h3>
      {children}
    </div>
  );

  return (
    <section
      id="contact"
      className="relative py-12 sm:py-16 lg:py-24 px-4 sm:px-6 lg:px-8 bg-gradient-to-b from-secondary-dark to-secondary overflow-hidden"
    >
      <div className="max-w-7xl mx-auto relative">
        <div className="text-center mb-8 sm:mb-12 lg:mb-16">
        <h2 className="text-4xl md:text-5xl font-serif font-bold mb-12 text-center text-white relative">
          Visit Us
          <div className="relative">
            <span className="absolute -bottom-3 left-1/2 transform -translate-x-1/2 w-20 h-0.5 bg-primary-light rounded-full"></span>
          </div>
        </h2>
        <p className="max-w-2xl mx-auto text-white/80 text-lg">
          We&apos;d love to see you at The Way Coffee House. Drop by for a coffee, attend one of our events, or get in touch to learn more.
        </p>
        </div>

        <div className="grid lg:grid-cols-3 gap-6 lg:gap-8">
          {/* Left Column - Info Cards */}
          <div className="space-y-6 lg:space-y-8">
            {/* Opening Hours */}
            <InfoCard
              title="Opening Hours"
              icon={<FaClock className="text-primary-light" />}
            >
              <div className="space-y-1 sm:space-y-2 max-w-full overflow-hidden">
                {OPENING_HOURS.map(({ day, time, isLate, isSunday }) => (
                  <div
                    key={day}
                    className={`flex justify-between items-center p-1.5 sm:p-2 rounded-lg transition-colors duration-200
                      ${isLate ? "bg-white/10" : "hover:bg-white/5"}
                      ${isSunday ? "bg-[#D4A574]/10" : ""}`}
                  >
                    <span className="font-medium text-white text-xs xs:text-sm sm:text-base mr-1.5">
                      {day}
                    </span>
                    <div className="text-right flex-shrink-0">
                      <span
                        className={`${
                          isSunday
                            ? "text-[#D4A574] font-medium"
                            : "text-white/90"
                        } text-xs xs:text-sm sm:text-base break-normal`}
                      >
                        {time}
                      </span>
                      {isLate && (
                        <span className="block text-[10px] xs:text-xs sm:text-sm text-[#D4A574] mt-0.5 font-medium">
                          Evening Events
                        </span>
                      )}
                    </div>
                  </div>
                ))}
              </div>
            </InfoCard>

            {/* Contact Information */}
            <InfoCard
              title="Get in Touch"
              icon={<FaPhone className="text-primary-light" />}
            >
              <div className="space-y-4">
                {CONTACT_INFO.map((item) => (
                  <a
                    key={item.title}
                    href={item.href}
                    aria-label={item.ariaLabel}
                    className="flex items-center gap-3 p-2 rounded-xl hover:bg-white/5 transition-all duration-300 group"
                  >
                    <div className="w-10 h-10 sm:w-12 sm:h-12 rounded-full bg-primary/10 flex items-center justify-center group-hover:bg-primary/20 shrink-0">
                      {item.icon}
                    </div>
                    <div className="flex-1 min-w-0">
                      <p className="text-primary-light text-xs sm:text-sm font-medium mb-1">
                        {item.title}
                      </p>
                      <p className="text-white/90 group-hover:text-white text-sm sm:text-base truncate">
                        {item.content}
                      </p>
                    </div>
                  </a>
                ))}

                {/* Social Media Links */}
                <div className="flex items-center justify-center gap-4 sm:gap-6 pt-6 mt-6 border-t border-white/10">
                  {SOCIAL_LINKS.map((social) => (
                    <a
                      key={social.label}
                      href={social.href}
                      aria-label={social.ariaLabel}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="w-10 h-10 sm:w-12 sm:h-12 rounded-full bg-white/5 flex items-center justify-center hover:bg-primary hover:scale-110 transition-all duration-300 group"
                    >
                      <div className="text-lg sm:text-xl text-gray-400 group-hover:text-white">
                        {social.icon}
                      </div>
                    </a>
                  ))}
                </div>
              </div>
            </InfoCard>
          </div>

          {/* Map - Spans 2 columns on desktop */}
          <div className="lg:col-span-2 h-[300px] sm:h-[400px] lg:h-full min-h-[500px]">
            <div className="bg-white/5 backdrop-blur-lg p-2 rounded-2xl border border-primary/20 shadow-lg transition-all duration-300 h-full overflow-hidden">
              <iframe
                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCWoKnIqMOJ1npXjMd3GXbknzUSLWYga0Q&q=The+Way+Coffee+House,Dunstable+UK&zoom=16"
                width="100%"
                height="100%"
                style={{ border: 0 }}
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
                title="The Way Coffee House location in Dunstable"
                className="rounded-xl filter brightness-95 contrast-105 w-full"
                aria-label="Map showing The Way Coffee House location at 6 West Street, Dunstable"
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContactSection;
