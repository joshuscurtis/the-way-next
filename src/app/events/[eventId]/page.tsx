import { notFound } from 'next/navigation';
import { getEvent, formatFrequency } from '@/data/events';
import { Metadata } from 'next';
import { Calendar, Clock, MapPin, ArrowLeft } from "lucide-react";
import Link from "next/link";
import Image from "next/image";

// Cache event pages for faster performance
export const revalidate = 3600; // Revalidate once per hour

type Props = {
  params: Promise<{ eventId: string }>;
  searchParams?: Promise<{ [key: string]: string | string[] | undefined }>;
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const resolvedParams = await params;
  const event = getEvent(resolvedParams.eventId);
  
  if (!event) {
    return {
      title: 'Event Not Found',
    };
  }

  return {
    title: `${event.title} | The Way Coffee House`,
    description: event.description,
    openGraph: {
      title: event.title,
      description: event.description,
      type: 'website',
      images: event.imageUrl ? [event.imageUrl] : undefined,
    },
    // Using script tags in the page for structured data instead
    alternates: {
      canonical: `/events/${event.id}`,
    }
  };
}

export default async function EventPage({ params }: Props) {
  const resolvedParams = await params;
  const event = getEvent(resolvedParams.eventId);

  if (!event) {
    notFound();
  }

  return (
    <div className="bg-gradient-to-b from-secondary-dark to-secondary min-h-screen py-16 px-6 md:px-8">
      <div className="max-w-4xl mx-auto">
        {/* Add JSON-LD structured data */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              '@context': 'https://schema.org',
              '@type': 'Event',
              name: event.title,
              description: event.description,
              image: event.imageUrl ? `https://theway.coffee${event.imageUrl}` : undefined,
              startDate: `2024-01-01T${event.startTime}:00`,
              endDate: `2024-01-01T${event.endTime}:00`,
              eventStatus: 'https://schema.org/EventScheduled',
              eventAttendanceMode: 'https://schema.org/OfflineEventAttendanceMode',
              location: {
                '@type': 'Place',
                name: event.location,
                address: {
                  '@type': 'PostalAddress',
                  streetAddress: 'Christ Church, West Street',
                  addressLocality: 'Dunstable',
                  addressRegion: 'Bedfordshire',
                  postalCode: 'LU6 1SX',
                  addressCountry: 'GB'
                }
              },
              organizer: {
                '@type': 'Organization',
                name: 'The Way Coffee House',
                url: 'https://theway.coffee'
              },
              offers: {
                '@type': 'Offer',
                price: '0',
                priceCurrency: 'GBP',
                availability: 'https://schema.org/InStock',
                validFrom: '2024-01-01'
              }
            })
          }}
        />
        
        <Link 
          href="/#events" 
          className="inline-flex items-center gap-2 text-white/80 hover:text-primary-light mb-8 transition-colors"
        >
          <ArrowLeft size={16} />
          <span>Back to events</span>
        </Link>

        <h1 className="text-4xl md:text-5xl font-serif font-bold mb-6 text-white relative">
          {event.title}
          <div className="relative">
            <span className="absolute -bottom-4 left-0 w-24 h-1 bg-primary-light rounded-full"></span>
          </div>
        </h1>
        
        <div className="mt-12 grid grid-cols-1 md:grid-cols-3 gap-8">
          <div className="md:col-span-2">
            {event.imageUrl && (
              <div className="relative w-full h-72 md:h-96 mb-8 rounded-xl overflow-hidden">
                <Image
                  src={event.imageUrl}
                  alt={event.imageAlt || `${event.title} event at The Way Coffee House`}
                  fill
                  priority
                  className="object-cover"
                />
                <div className="absolute inset-0 bg-gradient-to-t from-black/50 to-transparent opacity-60"></div>
              </div>
            )}
            
            <div className="p-8 rounded-2xl border border-primary/20 
              bg-secondary-light/50 backdrop-blur-lg animate-fade-in shadow-lg">
              
              <div className="prose prose-lg prose-invert max-w-none">
                {event.richDescription ? (
                  <div className="text-white/90 leading-relaxed text-lg" 
                       dangerouslySetInnerHTML={{ 
                         __html: event.richDescription.replace(/\n\n/g, '<br/><br/>').replace(/- /g, '• ') 
                       }}>
                  </div>
                ) : (
                  <p className="text-white/90 leading-relaxed text-lg">
                    {event.description}
                  </p>
                )}
              </div>
            </div>
          </div>
          
          <div className="space-y-4">
            <div className="p-6 rounded-2xl border border-primary/20 bg-secondary-light/50 backdrop-blur-lg shadow-lg">
              <h3 className="text-xl font-medium text-white mb-4">Event Details</h3>
              
              <div className="space-y-6">
                <div className="flex items-start gap-3 text-white/90">
                  <Calendar className="text-primary-light flex-shrink-0 mt-1" size={20} />
                  <div>
                    <p className="font-medium mb-1">When</p>
                    <p className="text-white/70">
                      {formatFrequency(event.dates, event.frequency)}
                    </p>
                  </div>
                </div>
                
                <div className="flex items-start gap-3 text-white/90">
                  <Clock className="text-primary-light flex-shrink-0 mt-1" size={20} />
                  <div>
                    <p className="font-medium mb-1">Time</p>
                    <p className="text-white/70">{event.startTime} - {event.endTime}</p>
                  </div>
                </div>
                
                <div className="flex items-start gap-3 text-white/90">
                  <MapPin className="text-primary-light flex-shrink-0 mt-1" size={20} />
                  <div>
                    <p className="font-medium mb-1">Location</p>
                    <p className="text-white/70">{event.location}</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div className="p-6 rounded-2xl border border-primary/20 bg-secondary-light/50 backdrop-blur-lg shadow-lg">
              <h3 className="text-xl font-medium text-white mb-4">Contact Us</h3>
              <p className="text-white/70 mb-4">
                Have questions about this event? Get in touch with us.
              </p>
              <Link 
                href="/#contact"
                className="block w-full py-3 bg-primary hover:bg-primary-dark text-white text-center rounded-xl transition-colors border border-primary-light/30"
              >
                Contact Information
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}