export default function LoadingGallery() {
    return (
      <section className="py-16 px-6 md:px-8 bg-gradient-to-b from-slate-900 to-slate-800">
        <div className="max-w-6xl mx-auto">
          <div className="grid grid-cols-3 gap-2 md:gap-8 max-w-4xl mx-auto">
            {[...Array(9)].map((_, i) => (
              <div
                key={i}
                className="relative aspect-square overflow-hidden rounded-2xl 
                  border border-white/10 bg-white/5 backdrop-blur-lg animate-pulse"
              >
                <div className="absolute inset-0 bg-gray-700/50" />
              </div>
            ))}
          </div>
        </div>
      </section>
    );
  }