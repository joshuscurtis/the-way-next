import type { Config } from "tailwindcss";

export default {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    screens: {
      'xs': '375px',
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    extend: {
      colors: {
        background: "var(--background)",
        foreground: "var(--foreground)",
        primary: {
          DEFAULT: "#1e40af", // Blue-800
          light: "#3b82f6", // Blue-500
          dark: "#1e3a8a", // Blue-900
        },
        secondary: {
          DEFAULT: "#0f172a", // Slate-900
          light: "#1e293b", // Slate-800
          dark: "#020617", // Slate-950
        },
        neutral: {
          DEFAULT: "#f1f5f9", // Slate-100
          light: "#f8fafc", // Slate-50
          dark: "#0f172a", // Slate-900
        },
      },
      fontFamily: {
        sans: ['Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
        serif: ['Georgia', 'serif'],
        display: ['Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
      },
      spacing: {
        'safe': 'env(safe-area-inset-bottom)',
      },
      height: {
        'screen-small': '100vh',
        'screen-dynamic': 'calc(100vh - env(safe-area-inset-bottom))',
        'dvh-full': '100dvh',
      },
    },
  },
  plugins: [],
} satisfies Config;
