import Link from 'next/link';
import { FaCoffee, FaFacebook, FaInstagram, FaMapMarkerAlt } from 'react-icons/fa';

const Footer = () => {
  const currentYear = new Date().getFullYear();
  
  return (
    <footer className="relative w-full bg-gradient-to-b from-secondary-dark to-secondary shadow-lg pb-safe">
      {/* Main Footer Content */}
      <div className="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8 py-8 sm:py-12">
        <div className="grid grid-cols-1 xs:grid-cols-2 lg:grid-cols-4 gap-6 xs:gap-8 lg:gap-12 mb-8 sm:mb-12">
          {/* Brand Column */}
          <div className="flex flex-col space-y-3 sm:space-y-4 lg:col-span-2 xs:col-span-2">
            <div className="flex items-center gap-3">
              <FaCoffee className="text-primary-light text-xl sm:text-2xl" />
              <h3 className="text-white font-serif text-lg sm:text-xl font-bold">The Way Coffee House</h3>
            </div>
            <p className="text-gray-300 leading-relaxed text-sm sm:text-base">
              Award-winning Fairtrade coffee, homemade cakes, and a welcoming community space in the heart of Dunstable.
            </p>
            <div className="flex items-center gap-2 text-gray-300">
              <FaMapMarkerAlt className="text-primary-light flex-shrink-0" />
              <p className="text-xs sm:text-sm">6 West Street, Dunstable, Bedfordshire, LU6 1JL</p>
            </div>
            <div className="flex gap-4 mt-1 sm:mt-2">
              <a 
                href="https://instagram.com/the_way_dunstable" 
                target="_blank"
                rel="noopener noreferrer"
                aria-label="Follow us on Instagram"
                className="text-gray-400 hover:text-primary-light transition-colors p-2 -m-2"
              >
                <FaInstagram className="text-lg sm:text-xl" />
              </a>
              <a 
                href="https://facebook.com/thewaydunstable" 
                target="_blank"
                rel="noopener noreferrer"
                aria-label="Like us on Facebook"
                className="text-gray-400 hover:text-primary-light transition-colors p-2 -m-2"
              >
                <FaFacebook className="text-lg sm:text-xl" />
              </a>
            </div>
          </div>
          
          {/* Quick Links */}
          <div>
            <h4 className="text-white text-base sm:text-lg font-medium mb-2 sm:mb-4">Quick Links</h4>
            <ul className="space-y-1 sm:space-y-2">
              <li>
                <Link href="/#events" className="text-gray-300 hover:text-primary-light transition-colors inline-block py-1 text-sm sm:text-base">
                  Events
                </Link>
              </li>
              <li>
                <Link href="/#gallery" className="text-gray-300 hover:text-primary-light transition-colors inline-block py-1 text-sm sm:text-base">
                  Gallery
                </Link>
              </li>
              <li>
                <Link href="/#contact" className="text-gray-300 hover:text-primary-light transition-colors inline-block py-1 text-sm sm:text-base">
                  Contact Us
                </Link>
              </li>
              <li>
                <a 
                  href="https://christchurchdunstable.org.uk/" 
                  className="text-gray-300 hover:text-primary-light transition-colors inline-block py-1 text-sm sm:text-base"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Christ Church Dunstable
                </a>
              </li>
            </ul>
          </div>
          
          {/* Opening Hours Summary */}
          <div>
            <h4 className="text-white text-base sm:text-lg font-medium mb-2 sm:mb-4">Opening Hours</h4>
            <ul className="space-y-1 text-xs sm:text-sm">
              <li className="text-gray-300">Monday-Saturday: 8:30am-3:00pm</li>
              <li className="text-gray-300">Wednesday: Open until 9:00pm</li>
              <li className="text-primary-light mt-2">
                <Link href="/#contact" className="hover:underline inline-block py-1">
                  View Full Schedule
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>

      {/* Copyright Bar */}
      <div className="border-t border-white/10 backdrop-blur-sm">
        <div className="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8 py-4 sm:py-6">
          <div className="flex flex-col sm:flex-row justify-between items-center gap-3 sm:gap-4">
            <p className="text-gray-400 text-xs sm:text-sm md:text-base text-center sm:text-left">
              © {currentYear} The Way Coffee House, Dunstable. All rights reserved.
            </p>
            <div className="flex gap-4 sm:gap-6">
              <a
                href="https://christchurchdunstable.org.uk/privacy/"
                className="text-gray-400 text-xs sm:text-sm md:text-base hover:text-primary-light transition-colors py-1"
                target="_blank"
                rel="noopener noreferrer"
              >
                Privacy Policy
              </a>
              <a
                href="https://christchurchdunstable.org.uk/what-we-do/safeguarding-2/"
                className="text-gray-400 text-xs sm:text-sm md:text-base hover:text-primary-light transition-colors py-1"
                target="_blank"
                rel="noopener noreferrer"
              >
                Safeguarding
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;