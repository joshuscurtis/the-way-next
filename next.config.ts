import type { NextConfig } from "next";

const nextConfig: NextConfig = {
  // Performance optimizations for Next.js
  output: 'standalone', // Optimized for production deployments
  
  // Image optimization
  images: {
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048],
    formats: ["image/webp", "image/avif"],
    minimumCacheTTL: 3600, // Increased cache time for better performance
    dangerouslyAllowSVG: true,
    contentDispositionType: "attachment",
    contentSecurityPolicy: "default-src 'self'; script-src 'none'; sandbox;",
    domains: [],
    remotePatterns: [],
  },

  // Performance optimizations
  poweredByHeader: false,
  compress: true,
  
  // Configure cross-origin isolation
  crossOrigin: 'anonymous',
  
  // Improve compilation performance
  reactStrictMode: true,
  
  // Generate a sitemap and robots.txt automatically
  experimental: {
    scrollRestoration: true, // Improve UX by restoring scroll position
  },

  // Security headers
  async headers() {
    return [
      {
        source: "/:path*",
        headers: [
          {
            key: "X-DNS-Prefetch-Control",
            value: "on",
          },
          {
            key: "Strict-Transport-Security",
            value: "max-age=63072000; includeSubDomains; preload",
          },
          {
            key: "X-Content-Type-Options",
            value: "nosniff",
          },
          {
            key: "X-Frame-Options",
            value: "SAMEORIGIN",
          },
          {
            key: "X-XSS-Protection",
            value: "1; mode=block",
          },
          {
            key: "Referrer-Policy",
            value: "strict-origin-when-cross-origin",
          },
          {
            key: "Permissions-Policy",
            value: "camera=(), microphone=(), geolocation=(self), interest-cohort=()",
          },
        ],
      },
      {
        // Apply cache headers for static assets
        source: "/static/:path*",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          }
        ],
      },
      {
        // Cache images aggressively
        source: "/:path*.(jpg|jpeg|gif|png|svg|webp|avif)",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          }
        ],
      },
      {
        // Cache fonts aggressively
        source: "/:path*.(woff|woff2|ttf|otf|eot)",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          }
        ],
      }
    ];
  },
};
export default nextConfig;
