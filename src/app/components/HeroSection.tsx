"use client";
import Image from "next/image";
import { useEffect, useState } from "react";
import { FaChevronDown } from "react-icons/fa";

const HeroSection = () => {
  const [currentImage, setCurrentImage] = useState(0);
  const [scrollY, setScrollY] = useState(0);
  const [isClient, setIsClient] = useState(false);

  // Set client-side flag to prevent hydration mismatch
  useEffect(() => {
    setIsClient(true);
  }, []);

  // Handle scroll effects
  useEffect(() => {
    const handleScroll = () => setScrollY(window.scrollY);
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  const heroImages = [
    "/min/snow-outside-min.webp",
    "/min/coffeemachine-min.webp",
    "/min/cake-min.webp",
    "/min/coffee-min.webp",
  ];

  // Image rotation effect
  useEffect(() => {
    if (!isClient) return; // Skip on server-side
    
    const interval = setInterval(() => {
      setCurrentImage((prev) => (prev + 1) % heroImages.length);
    }, 5000);
    
    return () => clearInterval(interval);
  }, [heroImages.length, isClient]);

  return (
    <section id="hero" className="relative h-screen-dynamic w-full overflow-hidden">
      <div
        className="absolute inset-0 will-change-transform"
        style={{ transform: isClient ? `translateY(${scrollY * 0.5}px)` : 'none' }}
      >
        {heroImages.map((img, index) => (
          <div 
            key={img}
            className={`absolute inset-0 transition-opacity duration-1000 ${
              index === (isClient ? currentImage : 0) ? "opacity-100" : "opacity-0"
            }`}
          >
            <Image
              src={img}
              alt={`The Way Coffee House ${index + 1}`}
              fill
              priority={index === 0}
              sizes="100vw"
              className="object-cover brightness-[0.65]"
            />
          </div>
        ))}
        <div className="absolute inset-0 bg-gradient-to-b from-black/30 via-transparent to-black/60" />
      </div>

      <div className="absolute inset-0 flex flex-col items-center justify-center text-white px-4 sm:px-6 pb-20">
        <div 
          className="backdrop-blur-md bg-[#0A2240] p-6 xs:p-8 sm:p-10 md:p-16 pb-10 sm:pb-12 md:pb-32 rounded-xl aspect-[3/4] w-[280px] sm:w-[300px] md:w-[350px] text-center mb-6 sm:mb-8 shadow-2xl transform hover:scale-105 transition-all duration-500 outline outline-white outline-offset-[-8px] sm:outline-offset-[-10px] animate-fade-in relative"
        >
          <h1 className="text-white text-5xl xs:text-6xl sm:text-7xl font-bold mt-8 sm:mt-10 md:mt-2 text-center font-display tracking-wide uppercase">
            The
          </h1>
          <h1 className="text-white text-5xl xs:text-6xl sm:text-7xl font-bold mb-6 xs:mb-8 md:mb-2 text-center font-display tracking-wide uppercase">
            Way
          </h1>
          <p className="text-white text-xl xs:text-2xl md:text-3xl font-bold pt-4 xs:pt-6 md:pt-16 font-display tracking-wider">
            Coffee House
          </p>
          <a 
            href="https://christchurchdunstable.org.uk/" 
            rel="noopener noreferrer" 
            className="absolute bottom-2 right-2 pb-2 pr-2 text-white/60 text-xs"
          >
            Christ Church Dunstable
          </a>
        </div>

        <p
          className="text-lg xs:text-xl sm:text-2xl md:text-3xl font-light text-center max-w-[95%] xs:max-w-[90%] sm:max-w-3xl mb-8 sm:mb-10
          drop-shadow-xl leading-relaxed tracking-wide
          text-white bg-secondary-dark/70 backdrop-blur-md rounded-xl p-4 sm:p-6 border border-primary-light/20"
        >
          Quality coffee, homemade cakes, and a welcoming community space in the
          heart of Dunstable.
        </p>

        <button
          onClick={() => {
            const element = document.getElementById("events");
            element?.scrollIntoView({
              behavior: "smooth",
            });
          }}
          className="px-6 sm:px-8 py-3 sm:py-4 bg-primary hover:bg-primary-dark backdrop-blur-md
              rounded-full text-white transition-all transform hover:scale-105
              text-base sm:text-lg font-medium shadow-lg hover:shadow-xl border border-primary-light/30
              whitespace-nowrap"
        >
          Discover More
        </button>

        <div className="absolute bottom-8 sm:bottom-10 left-1/2 transform -translate-x-1/2 animate-bounce flex items-center justify-center">
          <FaChevronDown className="text-xl sm:text-2xl text-white drop-shadow-lg" />
        </div>
      </div>
    </section>
  );
};

export default HeroSection;