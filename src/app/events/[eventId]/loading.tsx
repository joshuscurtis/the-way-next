export default function EventLoading() {
  return (
    <div className="bg-gradient-to-b from-secondary-dark to-secondary min-h-screen py-16 px-6 md:px-8">
      <div className="max-w-4xl mx-auto">
        <div className="h-6 w-24 bg-primary/20 rounded mb-8 animate-pulse" />

        <div className="h-14 w-64 bg-primary/20 rounded-xl mb-12 animate-pulse" />
        
        <div className="mt-12 grid grid-cols-1 md:grid-cols-3 gap-8">
          <div className="md:col-span-2">
            <div className="w-full h-72 md:h-96 mb-8 rounded-xl bg-primary/10 animate-pulse" />
            
            <div className="p-8 rounded-2xl border border-primary/20 bg-secondary-light/50 backdrop-blur-lg shadow-lg animate-pulse">
              <div className="space-y-4">
                <div className="h-4 bg-primary/15 rounded-full w-full" />
                <div className="h-4 bg-primary/15 rounded-full w-5/6" />
                <div className="h-4 bg-primary/15 rounded-full w-full" />
                <div className="h-4 bg-primary/15 rounded-full w-4/5" />
                <div className="h-4 bg-primary/15 rounded-full w-full" />
              </div>
            </div>
          </div>
          
          <div className="space-y-4">
            <div className="p-6 rounded-2xl border border-primary/20 bg-secondary-light/50 backdrop-blur-lg shadow-lg animate-pulse">
              <div className="h-7 w-36 bg-primary/20 rounded-lg mb-6" />
              
              <div className="space-y-6">
                {Array.from({ length: 3 }).map((_, i) => (
                  <div key={i} className="flex items-start gap-3">
                    <div className="w-5 h-5 rounded-full bg-primary-light/50 mt-1" />
                    <div className="space-y-2 flex-1">
                      <div className="h-4 w-20 bg-primary/20 rounded" />
                      <div className="h-4 w-3/4 bg-primary/15 rounded" />
                    </div>
                  </div>
                ))}
              </div>
            </div>
            
            <div className="p-6 rounded-2xl border border-primary/20 bg-secondary-light/50 backdrop-blur-lg shadow-lg">
              <div className="h-7 w-36 bg-primary/20 rounded-lg mb-4" />
              <div className="h-4 w-full bg-primary/15 rounded mb-6" />
              <div className="h-10 w-full bg-primary/30 rounded-xl" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}