import { MetadataRoute } from 'next';
import { getAllEvents } from '@/data/events';

export default function sitemap(): MetadataRoute.Sitemap {
  const baseUrl = 'https://thway.coffee';
  const events = getAllEvents();
  
  // Base pages
  const routes = [
    {
      url: baseUrl,
      lastModified: new Date(),
      changeFrequency: 'weekly' as const,
      priority: 1,
    },
  ];
  
  // Event pages
  const eventRoutes = events.map(event => ({
    url: `${baseUrl}/events/${event.id}`,
    lastModified: new Date(),
    changeFrequency: 'weekly' as const,
    priority: 0.8,
  }));
  
  return [...routes, ...eventRoutes];
}