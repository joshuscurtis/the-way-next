import type { Metadata } from "next";
import { Geist, Geist_Mono } from "next/font/google";
import { Analytics } from "@vercel/analytics/react"
import { SpeedInsights } from "@vercel/speed-insights/next";
import "./globals.css";

const geistSans = Geist({
  variable: "--font-geist-sans",
  subsets: ["latin"],
});

const geistMono = Geist_Mono({
  variable: "--font-geist-mono",
  subsets: ["latin"],
});

export const metadata: Metadata = {
  metadataBase: new URL('https://theway.coffee'),
  title: "The Way Coffee House | Award-Winning Coffee & Community in Dunstable",
  description:
    "Visit The Way Coffee House in Dunstable for award-winning, Fairtrade coffee, homemade cakes, and light refreshments. Family-friendly community space with regular events. Open Mon-Sat.",
  keywords: ["coffee house dunstable", "fair trade coffee bedfordshire", "community cafe dunstable", "homemade cakes dunstable", "coffee events", "family friendly cafe"],
  alternates: {
    canonical: '/',
  },
  robots: {
    index: true,
    follow: true,
    googleBot: {
      index: true,
      follow: true,
      'max-image-preview': 'large',
      'max-video-preview': -1,
      'max-snippet': -1,
    },
  },
  openGraph: {
    title: "The Way Coffee House | Award-Winning Coffee in Dunstable",
    description: "Visit our welcoming coffee house for award-winning Fairtrade coffee, homemade cakes, and a vibrant community space. Regular events for all ages.",
    url: 'https://theway.coffee',
    siteName: 'The Way Coffee House',
    images: [
      {
        url: "/min/coffee-min.webp",
        width: 1200,
        height: 630,
        alt: "The Way Coffee House in Dunstable",
      }
    ],
    locale: "en_GB",
    type: "website",
  },
  twitter: {
    card: "summary_large_image",
    title: "The Way Coffee House | Dunstable",
    description: "Award-winning Fairtrade coffee, homemade cakes, and a vibrant community space in Dunstable.",
    images: ["/min/coffee-min.webp"],
    creator: "@thewaydunstable",
  },
  authors: [{ name: "Christ Church Dunstable", url: "https://christchurchdunstable.org.uk" }],
  category: 'Coffee House',
  verification: {
    google: "verification_token", // Replace with actual Google verification token when available
  },
  // Using standard tags for SEO
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="scroll-smooth">
      <head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, viewport-fit=cover, shrink-to-fit=no, maximum-scale=5"
        />
        <meta name="format-detection" content="telephone=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta
          name="theme-color"
          content="#0A2240"
          media="(prefers-color-scheme: dark)"
        />
        <meta
          name="theme-color"
          content="#1e40af"
          media="(prefers-color-scheme: light)"
        />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="application-name" content="The Way Coffee" />
        <meta name="apple-mobile-web-app-title" content="The Way Coffee" />

        {/* PWA support */}
        <link rel="manifest" href="/manifest.json" />
        <link rel="apple-touch-icon" href="/apple-touch-icon.png" />

        {/* Resource hints for performance optimization */}
        <link rel="preconnect" href="https://maps.googleapis.com" crossOrigin="anonymous" />
        <link rel="preconnect" href="https://maps.gstatic.com" crossOrigin="anonymous" />
        <link rel="preconnect" href="https://fonts.googleapis.com" crossOrigin="anonymous" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="anonymous" />
        <link rel="dns-prefetch" href="https://instagram.com" />
        <link rel="dns-prefetch" href="https://facebook.com" />
        <link rel="dns-prefetch" href="https://youtube.com" />
      </head>
      <body
        className={`${geistSans.variable} ${geistMono.variable} antialiased`}
        suppressHydrationWarning
      >
        <script
          dangerouslySetInnerHTML={{
            __html: `
              (function() {
                // Handle dark mode
                if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                  document.documentElement.classList.add('dark')
                } else {
                  document.documentElement.classList.remove('dark')
                }
                
                // Handle Google Maps errors
                window.gm_authFailure = function() {
                  console.error('Google Maps failed to load');
                  const mapElements = document.querySelectorAll('[data-map-container]');
                  mapElements.forEach(el => {
                    el.innerHTML = '<div class="p-4 text-center">Map temporarily unavailable</div>';
                  });
                };
              })();
            `,
          }}
        />
        
        {/* Add JSON-LD structured data */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              '@context': 'https://schema.org',
              '@type': 'CafeOrCoffeeShop',
              '@id': 'https://theway.coffee/#coffeeshop',
              name: 'The Way Coffee House',
              description: 'A community coffee house in the heart of Dunstable. Serving award-winning Fairtrade coffee with a warm welcome.',
              image: 'https://theway.coffee/min/coffee-min.webp',
              url: 'https://theway.coffee',
              telephone: '+44 1582 661696',
              priceRange: '£',
              address: {
                '@type': 'PostalAddress',
                streetAddress: 'Christ Church, West Street',
                addressLocality: 'Dunstable',
                addressRegion: 'Bedfordshire',
                postalCode: 'LU6 1SX',
                addressCountry: 'GB'
              },
              geo: {
                '@type': 'GeoCoordinates',
                latitude: '51.8857',
                longitude: '-0.5209'
              },
              openingHoursSpecification: [
                {
                  '@type': 'OpeningHoursSpecification',
                  dayOfWeek: ['Monday', 'Tuesday', 'Thursday', 'Friday', 'Saturday'],
                  opens: '08:30',
                  closes: '15:00'
                },
                {
                  '@type': 'OpeningHoursSpecification',
                  dayOfWeek: 'Wednesday',
                  opens: '08:30',
                  closes: '21:00'
                }
              ],
              servesCuisine: ['Coffee', 'Cakes', 'Light refreshments'],
              amenityFeature: [
                { name: 'WiFi' },
                { name: 'Family friendly' },
                { name: 'Wheelchair accessible' }
              ]
            })
          }}
        />
        
        {children}
        <Analytics />
        <SpeedInsights />
      </body>
    </html>
  );
}
