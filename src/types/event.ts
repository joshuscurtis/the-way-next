import { LucideIcon } from "lucide-react";

export type FrequencyType = "weekly" | "monthly" | "term time";

export interface Event {
  id: string;
  title: string;
  dates: string; // e.g., "First Wednesday", "Every Friday"
  date?: string; // For specific date events
  startTime: string;
  endTime: string;
  location: string;
  description: string;
  frequency: FrequencyType;
  icon: LucideIcon;
  imageUrl?: string; // Optional image for event
  imageAlt?: string; // Detailed alt text for accessibility
  richDescription?: string; // HTML/Markdown formatted description
  eventType?: string; // Category of event (e.g., "games", "creative", "social")
}