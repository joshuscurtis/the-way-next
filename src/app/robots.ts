import { MetadataRoute } from 'next';

export default function robots(): MetadataRoute.Robots {
  return {
    rules: {
      userAgent: '*',
      allow: '/',
    },
    sitemap: 'https://theway.coffee/sitemap.xml',
    host: 'https://theway.coffee',
  };
}